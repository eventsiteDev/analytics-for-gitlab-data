require 'dotenv/load'
require './class/mongodb'
require './class/gitlab_api'


mongo_uri = ENV['MONGO_URI']
gitlab_token = ENV['GITLAB_TOKEN']

main_group_id = ENV['GROUP_ID']

base_gitlab_uri = 'https://gitlab.com/api/v4'


mongo = MongoDB.new(mongo_uri)
api = GitLabAPI.new(base_gitlab_uri, gitlab_token)



## Inicializar borrado
mongo.drop_collection("epics")
mongo.drop_collection("milestones")

epics = api.get_all_resources_by_group(main_group_id, "epics")
epics.each do |epic|
  mongo.save_object("epics", epic)
end

milestones = api.get_all_resources_by_group(main_group_id, "milestones")
milestones.each do |milestone|
  mongo.save_object("milestones", milestone)
end
