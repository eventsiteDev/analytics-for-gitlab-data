require 'dotenv/load'
require './class/mongodb'
require './class/gitlab_api'


mongo_uri = ENV['MONGO_URI']
gitlab_token = ENV['GITLAB_TOKEN']

main_group_id = ENV['GROUP_ID']

base_gitlab_uri = 'https://gitlab.com/api/v4'


mongo = MongoDB.new(mongo_uri)
api = GitLabAPI.new(base_gitlab_uri, gitlab_token)



## Inicializar borrado
[
  { date: "2023-01-01", name: "Año Nuevo" },
  { date: "2023-01-09", name: "Día de los Reyes Magos" },
  { date: "2023-03-20", name: "Día de San José" },
  { date: "2023-04-02", name: "Domingo de Ramos" },
  { date: "2023-04-06", name: "Jueves Santo" },
  { date: "2023-04-07", name: "Viernes Santo" },
  { date: "2023-04-09", name: "Domingo de Resurrección" },
  { date: "2023-05-01", name: "Día de Trabajo" },
  { date: "2023-05-22", name: "Día de la Ascensión" },
  { date: "2023-06-12", name: "Corpus Christi" },
  { date: "2023-06-19", name: "Sagrado Corazón" },
  { date: "2023-07-03", name: "San Pedro y San Pablo" },
  { date: "2023-07-20", name: "Día de la Independencia" },
  { date: "2023-08-07", name: "Batalla de Boyacá" },
  { date: "2023-08-21", name: "La asunción de la Virgen" },
  { date: "2023-10-16", name: "Día de la Raza" },
  { date: "2023-11-06", name: "Todos los Santos" },
  { date: "2023-11-13", name: "Independencia de Cartagena" },
  { date: "2023-12-08", name: "Día de la Inmaculada Concepción" },
  { date: "2023-12-25", name: "Día de Navidad" }
].each do |holiday|
  mongo.save_object("holidays", holiday)
end
