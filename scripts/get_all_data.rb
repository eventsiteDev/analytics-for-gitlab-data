require 'dotenv/load'
require './class/mongodb'
require './class/gitlab_api'


mongo_uri = ENV['MONGO_URI']
gitlab_token = ENV['GITLAB_TOKEN']
main_group_id = ENV['GROUP_ID']

base_gitlab_uri = 'https://gitlab.com/api/v4'


mongo = MongoDB.new(mongo_uri)
api = GitLabAPI.new(base_gitlab_uri, gitlab_token)


groups_valids = [10498543,51218687,51218669,51216950,16824323]

## Inicializar borrado
mongo.drop_collection("issues")
mongo.drop_collection("projects")
mongo.drop_collection("groups")

groups = api.get_groups
groups.each do |group|
  mongo.save_object("groups", group)
end


groups_valids.each do |group_id|
  projects = api.get_all_resources_by_group(group_id, "projects")
  projects.each do |project|
    mongo.save_object("projects", project)
    api.get_all_issues(project["id"], mongo)
  end
end

# mongo.drop_collection("members")
# members = api.get_all_resources_by_group(main_group_id, "members")
# members.each do |member|
#   mongo.save_object("members", member)
# end


puts Time.now
