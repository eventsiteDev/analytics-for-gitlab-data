require 'dotenv/load'
require './class/mongodb'
require './class/functions'


mongo_uri = ENV['MONGO_URI']
mongo = MongoDB.new(mongo_uri)


page = 1
per_page = 100
loop do
  epics = mongo.find_all('epics', page, per_page)
  if epics.empty?
    break
  else
    puts "Procesando #{epics.size} epics..."
    epics.each do |epic|
      # Aquí puedes hacer lo que necesites con cada documento
      puts "Procesando epic #{epic['labels'].count}..."
      product_label = find_first_product_label(epic)
      weight = mongo.sum_weight_by_epic_iid(epic['iid'])
      query = { _id: epic['_id'] }
      update = { '$set' => { product: product_label, weight: weight} }
      mongo.update_one('epics', query, update)
    end
    page += 1
  end
end



page = 1
per_page = 100
loop do
  issues = mongo.find_all('issues', page, per_page)
  if issues.empty?
    puts "break"
    break
  else
    puts "Procesando #{issues.size} issues..."
    issues.each do |issue|
      # Aquí puedes hacer lo que necesites con cada documento
      puts "Procesando issue #{issue['label_events'].count}..."
      count_returned = count_added_with_returned_status(issue['label_events'])
      award_emoji = find_award_emoji(issue['award_emojis'])
      epic = mongo.find_one("epics",{ iid: issue['epic_iid'] })
      product = epic['product'] if !epic.nil?

      query = { _id: issue['_id'] }
      update = { '$set' => {
                              count_returned: count_returned,
                              award_emoji: award_emoji,
                              product: product
                              }
                }
      mongo.update_one('issues', query, update)
    end
    page += 1
  end
end
