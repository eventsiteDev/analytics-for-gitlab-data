# Analytics for gitlab data

This project retrieves, stores and Analytics GitLab issues data in a MongoDB database.

## Getting Started

### Prerequisites

- Ruby 3.0 or higher
- MongoDB
- GitLab Personal Access Token (with `api` scope)
- GitLab Group ID

### Environment Variables

The following environment variables are required:

- `MONGO_URI`: the connection URI for the MongoDB instance
- `GITLAB_TOKEN`: the GitLab Personal Access Token
- `GROUP_ID`: the ID of the GitLab group that contains the project's issues

### Installing

1. Clone the repository
2. Install dependencies by running `bundle install`
3. Set the required environment variables
4. Run the script for  Functionality

## Functionality

### `get_all_data.rb`
```
bundle exec ruby get_all_issues.rb

```
- This script get issues
- this script get projects
- this script get groups

### `create_holidays.rb`

```
bundle exec ruby create_holidays.rb

```
### `get_base.rb`

```
bundle exec ruby get_base.rb

```

- This script get epics
- this script get milestones


### `reprocess_issues.rb`
```
bundle exec ruby reprocess_issues.rb

```
- Get the issues returned in test
- Assign the product type of the issue


## Querys

### issues_sin_sprint
### report_epics
### report_issues
### reporte_de_sprints

Get team productivity per sprint

| Milestone ID | Title                        | Start Date | Due Date   | Working Days | Issue Count | Total Weight | Weight per Day |
|--------------|------------------------------|------------|------------|--------------|-------------|--------------|----------------|
| 53           | 2023-1            | 2023-01-02 | 2023-01-06 | 5            | xxxxx       | xxxxx        | xxxxx          |
| 54           | 2023-2                       | 2023-01-10 | 2023-01-13 | 4            | xxxxx       | xxxxx        | xxxxx          |
| 55           | 2023-3                       | 2023-01-16 | 2023-01-20 | 5            | xxxxx       | xxxxx        | xxxxx          |
| 56           | 2023-4                       | 2023-01-23 | 2023-01-27 | 5            | xxxxx       | xxxxx        | xxxxx          |
| 57           | 2023-5                       | 2023-01-30 | 2023-02-10 | 10           | xxxxx       | xxxxx        | xxxxx          |
| 58           | 2023-6              | 2023-02-13 | 2023-02-24 | 10           | xxxxx       | xxxxx        | xxxxx          |
| 59           | 2023-7     | 2023-02-27 | 2023-03-10 | 10           | xxxxx       | xxxxx        | xxxxx          |
| 60           | 2023-8              | 2023-03-13 | 2023-03-17 | 5            | xxxxx       | xxxxx        | xxxxx          |
| 61           | 2023-9             | 2023-03-21 | 2023-04-05 | 12           | xxxxx       | xxxxx        | xxxxx          |
| 62           | 2023-10     | 2023-04-10 | 2023-04-14 | 5            | xxxxx       | xxxxx        | xxxxx          |
| 63           | 2023-11 | 2023-04-17 | 2023-04-29 | 11           | xxxxx       | xxxxx        | xxxxx          |

## Built With

- Ruby
- MongoDB
- GitLab API

## Authors

- Chatgpt
- Wgarcia

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
