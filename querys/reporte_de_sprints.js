## Traer los sprint

db.milestones.aggregate([
    {
        $match: {
            "start_date": {
                $gte: "2023-01-01",
                $lt: "2024-01-01"
            }
        }
    },
    {
        $lookup: {
            from: "issues",
            localField: "id",
            foreignField: "milestone_id",
            as: "issues"
        }
    },
    {
        $lookup: {
            from: "colombian_holidays",
            localField: "start_date",
            foreignField: "date",
            as: "holidays"
        }
    },
    {
        $unwind: {
            path: "$issues",
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $addFields: {
            days_difference: {
                $divide: [
                    {
                        $subtract: [
                            {
                                $dateFromString: {
                                    dateString: "$due_date"
                                }
                            },
                            {
                                $dateFromString: {
                                    dateString: "$start_date"
                                }
                            }
                        ]
                    },
                    86400000
                ]
            }
        }
    },
    {
        $addFields: {
            working_days: {
                $reduce: {
                    input: {
                        $range: [0, "$days_difference"]
                    },
                    initialValue: 1,
                    in: {
                        $let: {
                            vars: {
                                current_date: {
                                    $add: [
                                        {
                                            $dateFromString: {
                                                dateString: "$start_date"
                                            }
                                        },
                                        {
                                            $multiply: ["$$this", 86400000]
                                        }
                                    ]
                                }
                            },
                            in: {
                                $switch: {
                                    branches: [
                                        {
                                            case: {
                                                $eq: [
                                                    {
                                                        $dayOfWeek: "$$current_date"
                                                    },
                                                    1
                                                ]
                                            },
                                            then: "$$value"
                                        },
                                        {
                                            case: {
                                                $eq: [
                                                    {
                                                        $dayOfWeek: "$$current_date"
                                                    },
                                                    7
                                                ]
                                            },
                                            then: "$$value"
                                        },
                                        {
                                            case: {
                                                $in: [
                                                    "$$current_date",
                                                    "$holidays.date"
                                                ]
                                            },
                                            then: "$$value"
                                        }
                                    ],
                                    default: {
                                        $add: ["$$value", 1]
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },
{
    $group: {
      _id: "$id",
			milestone_id: { $first: "$iid" },
      title: { $first: "$title" },
      start_date: { $first: "$start_date" },
      due_date: { $first: "$due_date" },
      working_days: { $first: "$working_days" },
      issue_count: { $sum: 1 },
      total_weight: { $sum: "$issues.weight" }
    }
  },
  {
    $addFields: {
      weight_per_day: { $divide: ["$total_weight", "$working_days"] }
    }
  },
    {
        $project: {
            _id: 0,
						milestone_id: 1,
            title: 1,
            start_date: 1,
            due_date: 1,
            working_days: 1,
            issue_count: 1,
            total_weight: 1,
						weight_per_day: 1
        }
    },
    {
        $sort: {
            start_date: 1
        }
    }
]);

## Traer el listado de issues sin sprint


db.issues.aggregate([
  {
    $match: {
      "state": "closed",
      "closed_at": {
        $gte: "2023-01-01"
      },
      "milestone_id": null,
      "web_url": {
        $regex: "backend|frontend",
        $options: "i"
      }
    }
  },
  {
    $project: {
      _id: 0,
      title: "$title",
      state: "$state",
      created_at: "$created_at",
      closed_at: "$closed_at",
      milestone_id: "$milestone_id",
      project_id: "$project_id",
      web_url: "$web_url"
    }
  }
]);
