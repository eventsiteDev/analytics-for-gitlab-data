## Traer los sprint

db.airbyte_raw_group_milestones.aggregate([
    {
        $match: {
            "_airbyte_data.start_date": {
                $gte: "2023-01-01",
                $lt: "2024-01-01"
            }
        }
    },
    {
        $lookup: {
            from: "airbyte_raw_issues",
            localField: "_airbyte_data.id",
            foreignField: "_airbyte_data.milestone_id",
            as: "issues"
        }
    },
    {
        $lookup: {
            from: "colombian_holidays",
            localField: "_airbyte_data.start_date",
            foreignField: "date",
            as: "holidays"
        }
    },
    {
        $unwind: {
            path: "$issues",
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $addFields: {
            days_difference: {
                $divide: [
                    {
                        $subtract: [
                            {
                                $dateFromString: {
                                    dateString: "$_airbyte_data.due_date"
                                }
                            },
                            {
                                $dateFromString: {
                                    dateString: "$_airbyte_data.start_date"
                                }
                            }
                        ]
                    },
                    86400000
                ]
            }
        }
    },
    {
        $addFields: {
            working_days: {
                $reduce: {
                    input: {
                        $range: [0, "$days_difference"]
                    },
                    initialValue: 1,
                    in: {
                        $let: {
                            vars: {
                                current_date: {
                                    $add: [
                                        {
                                            $dateFromString: {
                                                dateString: "$_airbyte_data.start_date"
                                            }
                                        },
                                        {
                                            $multiply: ["$$this", 86400000]
                                        }
                                    ]
                                }
                            },
                            in: {
                                $switch: {
                                    branches: [
                                        {
                                            case: {
                                                $eq: [
                                                    {
                                                        $dayOfWeek: "$$current_date"
                                                    },
                                                    1
                                                ]
                                            },
                                            then: "$$value"
                                        },
                                        {
                                            case: {
                                                $eq: [
                                                    {
                                                        $dayOfWeek: "$$current_date"
                                                    },
                                                    7
                                                ]
                                            },
                                            then: "$$value"
                                        },
                                        {
                                            case: {
                                                $in: [
                                                    "$$current_date",
                                                    "$holidays.date"
                                                ]
                                            },
                                            then: "$$value"
                                        }
                                    ],
                                    default: {
                                        $add: ["$$value", 1]
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },
{
    $group: {
      _id: "$_airbyte_data.id",
			milestone_id: { $first: "$_airbyte_data.iid" },
      title: { $first: "$_airbyte_data.title" },
      start_date: { $first: "$_airbyte_data.start_date" },
      due_date: { $first: "$_airbyte_data.due_date" },
      working_days: { $first: "$working_days" },
      issue_count: { $sum: 1 },
      total_weight: { $sum: "$issues._airbyte_data.weight" }
    }
  },
  {
    $addFields: {
      weight_per_day: { $divide: ["$total_weight", "$working_days"] }
    }
  },
    {
        $project: {
            _id: 0,
						milestone_id: 1,
            title: 1,
            start_date: 1,
            due_date: 1,
            working_days: 1,
            issue_count: 1,
            total_weight: 1,
						weight_per_day: 1
        }
    },
    {
        $sort: {
            start_date: 1
        }
    }
]);

## Traer el listado de issues sin sprint


db.airbyte_raw_issues.aggregate([
  {
    $match: {
      "_airbyte_data.state": "closed",
      "_airbyte_data.closed_at": {
        $gte: "2023-01-01"
      },
      "_airbyte_data.milestone_id": null,
      "_airbyte_data.web_url": {
        $regex: "backend|frontend",
        $options: "i"
      }
    }
  },
  {
    $project: {
      _id: 0,
      title: "$_airbyte_data.title",
      state: "$_airbyte_data.state",
      created_at: "$_airbyte_data.created_at",
      closed_at: "$_airbyte_data.closed_at",
      milestone_id: "$_airbyte_data.milestone_id",
      project_id: "$_airbyte_data.project_id",
      web_url: "$_airbyte_data.web_url"
    }
  }
]);
