require 'httparty'
require 'json'

class GitLabAPI
  include HTTParty

  def initialize(base_uri, gitlab_token)
    @base_uri = base_uri
    @headers = { "Authorization" => "Bearer #{gitlab_token}" }
  end

  def get_groups
    url = "#{@base_uri}/groups"
    response = self.class.get(url, headers: @headers)

    if response.code == 200
      JSON.parse(response.body)
    else
      puts "Error al obtener los grupos: #{response.code} - #{response.message}"
    end
  end

  def get_all_issues(project_id, mongo)
      url = "#{@base_uri}/projects/#{project_id}/issues"
      page = 0
      since_date = '2022-10-01T00:00:00Z'  # filtro para obtener solo issues cerrados después de esta fecha

      loop do
        response = self.class.get(url, headers: @headers, query: { per_page: 100, page: page, state: 'closed', closed_at: since_date })
        if response.code == 200
          issues = JSON.parse(response.body)
          issues.each do |issue|
            self.save_issue_with_info(issue, mongo)
          end
          break if response.headers['x-next-page'].nil? || response.headers['x-next-page'] == ''
          page = response.headers['x-next-page'].to_i
        else
          puts "Error al obtener los issues: #{response.code} - #{response.message}"
          break
        end
      end
    end

  def get_generic_resource(resource_path, project_id, issue_id = nil, merge_request_id = nil)
   if issue_id.nil? && merge_request_id.nil?
     url = "#{@base_uri}/projects/#{project_id}/#{resource_path}"
   elsif issue_id.nil?
     url = "#{@base_uri}/projects/#{project_id}/merge_requests/#{merge_request_id}/#{resource_path}"
   else
     url = "#{@base_uri}/projects/#{project_id}/issues/#{issue_id}/#{resource_path}"
   end

   response = self.class.get(url, headers: @headers)

   if response.code == 200
     JSON.parse(response.body)
   else
     puts "Error al obtener los recursos: #{response.code} - #{response.message}"
     []
   end
 end

 def get_award_emojis(project_id, issue_id)
   get_generic_resource('award_emoji', project_id, issue_id)
 end

 def get_resource_label_events(project_id, issue_id)
   get_generic_resource('resource_label_events', project_id, issue_id)
 end

 def get_notes(project_id, issue_id)
   get_generic_resource('notes', project_id, issue_id)
 end

 def get_merge_requests(project_id)
   get_generic_resource('merge_requests', project_id)
 end

  def save_issue_with_info(issue, mongo)
    issue['award_emojis'] = get_award_emojis(issue['project_id'], issue['iid'])
    # issue['notes'] = get_notes(issue['project_id'], issue['iid'])
    issue['label_events'] = get_resource_label_events(issue['project_id'], issue['iid'])
    mongo.save_issue('issues', issue)
  end



  def get_all_resources_by_group(group_id, resource)
    url = "#{@base_uri}/groups/#{group_id}/#{resource}"
    resources = []

    page = 0
    loop do
      response = self.class.get(url, headers: @headers, query: { per_page: 100, page: page })

      if response.code == 200
        resources_json = JSON.parse(response.body)
        resources += resources_json
        break if response.headers['x-next-page'].nil? || response.headers['x-next-page'] == ''
        page = response.headers['x-next-page'].to_i
      else
        puts "Error al obtener los #{resource}: #{response.code} - #{response.message}"
        break
      end
    end

    resources
  end

end
