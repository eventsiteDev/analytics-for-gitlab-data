def count_added_with_returned_status(labels)
  count = 0
  labels.each do |label|
    if !label.nil? && label["action"] == "add" && !label["label"].nil? && label["label"]["name"] == "Dev-Status::Returned"
      count += 1
    end
  end
  count
end


def find_award_emoji(award_emojis)

  emoji_names = ['dart', 'writing_hand', 'broken_heart']

  award_emojis.each do |emoji|
    if !emoji.nil? && emoji['name'] && emoji_names.include?(emoji['name'])
      return emoji['name']
    end
  end

  return ''
end

def find_first_product_label(epic)
  # Assuming the 'labels' attribute is an array of strings
  labels = epic["labels"]

  # Find the first label containing 'Product::'
  product_label = labels.find { |label| label.downcase.start_with?('product::') }

  # Remove 'Product::' from the label, if found
  product_label = product_label.sub('Product::', '') if product_label

  product_label
end
