require 'mongo'

class MongoDB
  attr_accessor :client

  def initialize(mongo_uri)
    @client = Mongo::Client.new(mongo_uri)
  end

  def update_one(collection_name, query, update)
    @client[collection_name.to_sym].update_one(query, update)
  end

  def save_object(collection_name, object)
    @client[collection_name.to_sym].insert_one(object)
    puts "El objeto ha sido guardado en la colección '#{collection_name}' de MongoDB."
  end

  def drop_collection(collection_name)
    if @client.database.collection_names.include?(collection_name)
      @client[collection_name.to_sym].drop
      puts "La colección '#{collection_name}' ha sido eliminada de MongoDB."
    else
      puts "La colección '#{collection_name}' no existe en MongoDB."
    end
  end

  def find_all(collection_name, page = 1, per_page = 100)
    collection = @client[collection_name.to_sym]
    results = collection.find().skip((page - 1) * per_page).limit(per_page).to_a
    results
  end

  def sum_weight_by_epic_iid(epic_iid)
    collection = @client[:issues]
    result = collection.aggregate([
      {"$match": {"epic_iid": epic_iid}},
      {"$group": {"_id": "$epic_iid", "total_weight": {"$sum": "$weight"}}}
    ]).to_a.first
    result ? result["total_weight"] : 0
  end

  def find_one(collection_name, query)
    collection = @client[collection_name.to_sym]
    result = collection.find(query).limit(1).first
    result
  end

  def save_issue(collection_name, object)

    issue = self.find_one(collection_name, {id: object['id']})
    # puts issue
    if !issue.nil?
      self.update_one("issues", {_id: issue["_id"]} , object)
      puts "Actualizando #{issue["_id"]}"
    end
    if issue.nil?
      @client[collection_name.to_sym].insert_one(object)
      puts "creando"
    end
    #
    # @client[collection_name.to_sym].insert_one(object)
    # puts "El objeto ha sido guardado en la colección '#{collection_name}' de MongoDB."
  end

end
